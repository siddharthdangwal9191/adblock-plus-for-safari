# Adblock Plus for Safari (iOS)

## Building

### Requirements

- [Carthage](https://github.com/Carthage/Carthage)
- [Xcode](https://developer.apple.com/xcode/)
- [SwiftLint](https://github.com/realm/SwiftLint) (optional)

### Building in Xcode
*NOTE*: If you do not wish to make use of Firebase, remove the references from the project before building and skip to step 3 of the build instructions.

1. Copy the file `ABP-Secret-API-Env-Vars.sh` (available internally, sample file here(https://gitlab.com/eyeo/sandbox/adblock-plus-for-safari/snippets/1865852)) into the same directory as `AdblockPlusSafari.xcodeproj`.
2. Run `carthage update --platform ios` to install additional Swift dependencies.
3. Open `AdblockPlusSafari.xcodeproj` in Xcode.
4. Build & Run the project.
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.6] - 2020-12-01
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/10783d3959e48fea1fc358dd8e4420e0d0ee5b3f)

## [2.0.5] - 2020-12-01
### Added
* [Updated Filter Lists](https://gitlab.com/eyeo/sandbox/contentblockerlists/-/commit/af8d1d767cfb33e98319901e4649579eda60e0da)

## [2.0.4] - 2020-11-03
### Added
* Updated Filter Lists

## [2.0.3] - 2020-07-18
### Added
* Updated Filter Lists
* Updated French Translation

## [2.0.2] - 2020-06-30
### Added
* Onboarding now presents itself if app is reopened from background
* Minor UI tweaks
* Updated Filter Lists

## [2.0.1] - 2020-02-06
### Added
* Updated bundled filter lists

## [2.0.0] - 2019-12-11
### Added
App rewritten from scratch with the following features:
* A Redesign of the whole application
* Support for Dark Mode (iOS 13 only)
* User controlled domain whitelisting
* Acceptable Ads configuration toggle
* Opt-in Firebase Analyictics and crash reporting
* Safari Action Extension for whitelisting
/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

/// Datasource for whitelist NSCollectionView
class WhitelistItemDataSource {

    fileprivate let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)

    /// Checks NSUserDefaults for URL's added to the whitelist.
    ///
    /// - Returns: Either an Array with URL Strings or an empty array if no data found.
    func getWhitelistArray() -> [String] {
        let whitelistArray = groupDefaults?.stringArray(forKey: Constants.whitelistArray) ?? [String]()
        return whitelistArray
    }

    func appendToWhitelist(hostname: String) {
        var whitelistArray = self.getWhitelistArray()
        whitelistArray.append(hostname)
        groupDefaults?.set(whitelistArray, forKey: Constants.whitelistArray)
    }

    func whitelistItemForIndexPath(indexPath: IndexPath) -> String {
        let array = getWhitelistArray()
        return array[indexPath.item]
    }

    func removeURLfromArray(url: String) {
        var whitelistArray = self.getWhitelistArray()
        if let index = whitelistArray.firstIndex(of: url) {
            whitelistArray.remove(at: index)
            groupDefaults?.set(whitelistArray, forKey: Constants.whitelistArray)
            let whitelistManager = WhitelistManager()
            whitelistManager.applyWhitelist(whitelistArray)
        }
    }
    /// - Returns: Total number of whitelisted websites.
    func numberOfWhitelistItems() -> Int {
        let whitelistCount = getWhitelistArray().count
        return whitelistCount
    }
}
